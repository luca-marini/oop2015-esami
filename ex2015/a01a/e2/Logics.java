package a01a.e2;

import java.util.List;

public interface Logics {
	
	void hit(int pos);
	
	int getSize();
		
	List<Integer> getValues();
	
	int multiply();
	
	int min();
	
	int sum();
	
	void reset();

}
