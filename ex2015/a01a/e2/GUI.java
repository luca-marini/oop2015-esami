package a01a.e2;

import java.awt.BorderLayout;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import javax.swing.*;
import javax.swing.JPanel;

public class GUI extends JFrame{
	
	private Logics logic;
	private final int SIZE = 199;
	private List<JButton> jbs;
	
	public GUI(){
		// Inizializzazione base
		logic = new LogicsImpl(SIZE);
		JPanel center = new JPanel(new FlowLayout());
		ActionListener ac = (e -> {
			this.logic.hit(jbs.indexOf((JButton)e.getSource()));
			((JButton)e.getSource()).setEnabled(false);
		});
		jbs = Stream.generate(() -> new JButton())
					.limit(this.logic.getSize())
					.peek(center::add)
					.peek(b -> b.addActionListener(ac))
					.collect(Collectors.toCollection(ArrayList::new));
		
		IntStream.range(0, this.logic.getSize()).forEach(i -> jbs.get(i).setText(String.valueOf(this.logic.getValues().get(i))));
		
		this.setSize(500, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		
		
		// Pannello sud, ossia in basso
		final JPanel south = new JPanel(new FlowLayout());
		final JButton sum = new JButton("SUM");
		final JButton mult = new JButton("MULTIPLY");
		final JButton min = new JButton("MIN");
		south.add(sum);
		south.add(mult);
		south.add(min);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		
		sum.addActionListener(e -> {
			JOptionPane.showMessageDialog(this, "The result is " + String.valueOf(this.logic.sum()));
		
			this.jbs.forEach(b -> b.setEnabled(true));
		});
		mult.addActionListener(e -> { 
			JOptionPane.showMessageDialog(this, "The result is " + String.valueOf(this.logic.multiply()));
			this.jbs.forEach(b -> b.setEnabled(true));

		});
		min.addActionListener(e -> {
			JOptionPane.showMessageDialog(this, "The result is " + String.valueOf(this.logic.min()));
			this.jbs.forEach(b -> b.setEnabled(true));

		});
		
		// Pannello centrale

		this.getContentPane().add(BorderLayout.CENTER,center);
		this.setVisible(true);
	}
	
}
