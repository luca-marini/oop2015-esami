package a01a.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class LogicsImpl implements Logics {

	private List<Integer> val = new ArrayList<>();
	private List<Integer> operation = new ArrayList<>();
	
	public LogicsImpl(int size) {
		super();
		IntStream.range(0, size).filter(i -> i % 2 == 0).forEach(i -> val.add(i));
	}
	
	@Override
	public void hit(int pos) {
		this.operation.add(this.val.get(pos));
	}



	@Override
	public int multiply() {
		int res = 1;
		for(int v : this.operation) {
			res *= v;
		}
		return res;
	}

	@Override
	public int min() {
		int min = this.operation.stream()
							 .min((a,b) -> a-b)
							 .get();
		return min;
	}

	@Override
	public int sum() {
		int sum = this.operation.stream()
							    .mapToInt(v -> v.intValue())
							    .sum();
		return sum;
	}

	@Override
	public void reset() {
		this.operation.clear();
	}

	@Override
	public int getSize() {
		return this.val.size();
	}

	@Override
	public List<Integer> getValues() {
		return Collections.unmodifiableList(this.val);
	}

}
