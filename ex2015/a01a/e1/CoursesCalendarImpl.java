package a01a.e1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

public class CoursesCalendarImpl implements CoursesCalendar {
	
	private List<Integer> slots = new ArrayList<Integer>(Arrays.asList(9,10,11,12,14,15,16,17));
	private Map<Pair<Day, Room>, Set<Pair<Integer, String>>>calendar = new HashMap<>();
	private int ONEPM = 13;
	
	@Override
	public List<Integer> possibleSlots() {
		return Collections.unmodifiableList(this.slots);
	}

	@Override
	public void bookRoom(Day d, Room r, int start, int duration, String course) {
		Pair<Day, Room> p = new Pair<>(d, r);
		Set<Pair<Integer, String>> lessons = this.calendar.get(p);
		
		if(lessons == null) {
			this.calendar.put(p, new HashSet<Pair<Integer,String>>());
		} else {
			Long lo = lessons.stream().filter(l -> (l.getX() == start || l.getX() == start + duration)).count();
			if(lo > 0) {
				throw new IllegalStateException();
			}
		}
		if(start <= ONEPM && ONEPM < start + duration) {
			IntStream.range(start, start + duration + 1)
					 .filter(i -> i != 13)
			         .forEach(i -> this.calendar.get(p).add(new Pair<Integer, String>(i, course)));
		} else {
			IntStream.range(start, start + duration)
					 .forEach(i -> this.calendar.get(p).add(new Pair<Integer, String>(i, course)));
		}
	}

	@Override
	public Set<Pair<Integer, String>> dayRoomSlots(Day d, Room r) {
		if(!this.calendar.containsKey(new Pair<Day, Room>(d, r))) {
			return new HashSet<Pair<Integer,String>>();
		}
		return this.calendar.get(new Pair<>(d, r));
	}

	@Override
	public Map<Pair<Day, Room>, Set<Integer>> courseSlots(String course) {
		this.calendar.entrySet().stream()
								.filter(e -> e.getValue().forEach(i -> i.getY().equals(course)));
								
		return null;
	}

}
