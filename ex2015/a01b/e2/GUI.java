package a01b.e2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Example class.
 */
public class GUI extends JFrame {

    private static final int BUTTONS = 100;
    private Logics logic;
    private List<JButton> jbs;
    
    void enableButtons() {
    	this.jbs.stream().forEach(b -> b.setEnabled(true));
    }
    
    public GUI() {
    	  JPanel center = new JPanel(new FlowLayout());
    	this.logic = new LogicsImpl(BUTTONS);
    	jbs = Stream.generate(() -> new JButton()).limit(BUTTONS)
    				.peek(center::add)
    				.peek(b -> b.addActionListener(e -> {
    					b.setEnabled(false);
    					this.logic.hit(jbs.indexOf((JButton)e.getSource()));
    				}))
    				.collect(Collectors.toCollection(ArrayList::new));
    	IntStream.range(0, BUTTONS).forEach(i -> jbs.get(i).setText(this.logic.getValue(i)));
    	
        
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(600, 600);
        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());

        // Pannello sud, ossia in basso
        final JPanel south = new JPanel(new FlowLayout());
        final JButton lng = new JButton("OF LENGTH 4");
        final JButton sma = new JButton("< 1000");
        final JButton trap = new JButton("WITH TRAILING P");
        south.add(lng);
        south.add(sma);
        south.add(trap);
        canvas.add(BorderLayout.SOUTH, south);

        
        
        lng.addActionListener(e -> {
        	if(this.logic.getOperands().isEmpty()) {
        		JOptionPane.showMessageDialog(this, "INVALID");
        	} else if(this.logic.isAllEqualToLength()) {
        		JOptionPane.showMessageDialog(this, "TRUE");
        	} else {
        		JOptionPane.showMessageDialog(this, "FALSE");
        	}
        	enableButtons();
        	
        });
        sma.addActionListener(e -> {
        	if(this.logic.getOperands().isEmpty()) {
        		JOptionPane.showMessageDialog(this, "INVALID");
        	} else if(this.logic.isAllUnderValue()) {
        		JOptionPane.showMessageDialog(this, "TRUE");
        	} else {
        		JOptionPane.showMessageDialog(this, "FALSE");
        	}
        	enableButtons();

        });
        trap.addActionListener(e -> {
        	if(this.logic.getOperands().isEmpty()) {
        		JOptionPane.showMessageDialog(this, "INVALID");
        	} else if(this.logic.isAllEven()) {
        		JOptionPane.showMessageDialog(this, "TRUE");
        	} else {
        		JOptionPane.showMessageDialog(this, "FALSE");
        	}
        	enableButtons();

        });
      
        // Pannello centrale
      
	
        canvas.add(BorderLayout.CENTER, center);
        setLocationByPlatform(true);
        setContentPane(canvas);
        this.setVisible(true);
    }
    
  

}
