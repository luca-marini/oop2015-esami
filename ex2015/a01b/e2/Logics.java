package a01b.e2;

import java.util.List;

public interface Logics {

	void hit(int pos);
	
	String getValue(int pos);
	
	List<Integer> getOperands();
	
	boolean isAllEqualToLength();
	
	boolean isAllEven();
	
	boolean isAllUnderValue();
	
	void reset();
}
