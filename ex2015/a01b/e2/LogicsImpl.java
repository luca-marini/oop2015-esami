package a01b.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class LogicsImpl implements Logics {
	
	private List<Integer> val = new ArrayList<>();
	private List<Integer> operation = new ArrayList<>();
	private final int LIMITVALUE = 1000;
	
	public LogicsImpl(int size) {
		super();
		IntStream.range(0, size).forEach(i -> val.add(i * i));
	}

	@Override
	public void hit(int pos) {
		this.operation.add(this.val.get(pos));
	}

	@Override
	public String getValue(int pos) {
		return String.valueOf(this.val.get(pos)) + (this.val.get(pos) % 2 == 0 ? "p" : "d");
	}

	@Override
	public boolean isAllEqualToLength() {
		boolean b = ((int)this.operation.stream().filter(i -> i > 81 && i < 1024).count() == (this.operation.size()));
		this.reset();
		return b;
	}

	@Override
	public boolean isAllEven() {
		boolean b = ((int)this.operation.stream().filter(i -> i % 2 == 0).count() == this.operation.size());
		this.reset();
		return b;
	}

	@Override
	public boolean isAllUnderValue() {
		boolean b = ((int)this.operation.stream().filter(i -> i < LIMITVALUE).count() == this.operation.size());
		this.reset();
		return b;
	}

	@Override
	public void reset() {
		this.operation.clear();
	}

	@Override
	public List<Integer> getOperands() {
		return Collections.unmodifiableList(this.operation);
	}

}
