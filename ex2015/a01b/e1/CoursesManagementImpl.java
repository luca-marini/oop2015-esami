package a01b.e1;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CoursesManagementImpl implements CoursesManagement {
	
	private Map<Pair<Course, Integer>, Map<String, Set<String>>> courses = new HashMap<>();

	@Override
	public void assignProfessor(Course c, int year, String professor) {
		if(!(this.courses.get(new Pair<>(c, year)) == null)) {
			throw new IllegalStateException();
		}
		Map <String, Set<String>> prof = new HashMap<String, Set<String>>();
		prof.put(professor, new HashSet<String>());
		this.courses.put(new Pair<>(c, year), prof);
	}

	@Override
	public void assignTutor(Course c, int year, String professor) {
		
		if (this.courses.get(new Pair<>(c, year)) != null && this.courses.get(new Pair<>(c, year)).values().size() == TUTORING_SLOTS) {
//				this.courses.entrySet().stream().filter(e -> e.getKey().equals(new Pair<>(c, year)))
//												.map(e -> e.getValue().ge)
//												.peek(s -> s.);
//												
//				 ) {
			throw new IllegalStateException();
		}
		this.courses.entrySet().stream()
							   .filter(e -> e.getKey().equals(new Pair<>(c, year)))
							   .peek(e -> e.getValue().entrySet().stream()
							   .peek(e2 -> e2.getValue().add(professor)));
	}

	@Override
	public Set<Course> getUnassignedCourses(int year) {
		Set<Course> c = new HashSet<>();
		for(Course co : Course.values()) {
			c.add(co);
		}
		if(this.courses.isEmpty()) {
			
			return c;
		}
//		return this.courses.entrySet().stream()
//									  .filter(e -> e.getKey().getY().equals(year))
//									  .filter(e -> e.getValue().isEmpty())
//									  .map(e -> e.getKey().getX())
//									  .collect(Collectors.toSet());
		this.courses.entrySet().stream()
				  					  .filter(e -> e.getKey().getY().equals(year))
				  					  .peek(e -> c.remove(e.getKey().getX()));
				  					  
		return c;
	}

	@Override
	public Set<Pair<Integer, Course>> getCoursesByProf(String prof) {
		return this.courses.entrySet().stream()
									  .filter(e -> e.getValue().containsKey(prof))
									  .map(e -> new Pair<>(e.getKey().getY(), e.getKey().getX()))
									  .collect(Collectors.toSet());
	}

	@Override
	public Map<Integer, Set<Pair<Course, Integer>>> getRemainingTutoringSlots() {
		// TODO Auto-generated method stub
		return null;
	}

}
